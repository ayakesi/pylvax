from fabric.api import sudo, settings, cd, get, local, env, put, run

from maphub_fabric.utils import chown_home_folder

from .config import WEBAPP_NAME, DB_DIR, MEDIA_DIR
from .deploy import website_down, website_up


def db_backup():
    with settings(sudo_user=WEBAPP_NAME), cd(DB_DIR):
        sudo('./backup.sh')
        get('{}.sql'.format(WEBAPP_NAME), '../db')


def db_restore():
    if raw_input('Are you sure to restore database? [Y/N]: ').lower() != 'y':
        return

    website_down()

    with settings(sudo_user=WEBAPP_NAME), cd(DB_DIR):
        put('../db/{}.sql'.format(WEBAPP_NAME), '{}.sql'.format(WEBAPP_NAME))
        sudo('./restore.sh')
        run('rm {}.sql'.format(WEBAPP_NAME))

    website_up()



def media_backup():
    local('rsync -avz --delete {servername}:{media_dir}/ ../media/'.format(
          servername=env.host_string, media_dir=MEDIA_DIR))


def media_restore():
    local('rsync -avz --delete ../media/ {servername}:{media_dir}/'.format(
          servername=env.host_string, media_dir=MEDIA_DIR))
    chown_home_folder(WEBAPP_NAME)

