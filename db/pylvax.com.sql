--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--



SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


--
-- Name: pylvax_app_blog; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE pylvax_app_blog (
    id integer NOT NULL,
    datum timestamp with time zone NOT NULL,
    cim character varying(100) NOT NULL,
    leiras text NOT NULL,
    kep character varying(100) NOT NULL,
    _leiras_rendered text NOT NULL
);


--
-- Name: pylvax_app_blog_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pylvax_app_blog_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pylvax_app_blog_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pylvax_app_blog_id_seq OWNED BY pylvax_app_blog.id;


--
-- Name: pylvax_app_feladat; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE pylvax_app_feladat (
    id integer NOT NULL,
    datum date NOT NULL,
    slug character varying(50) NOT NULL,
    cim character varying(100) NOT NULL,
    leiras text NOT NULL,
    megoldas text NOT NULL,
    _megoldas_rendered text NOT NULL,
    _leiras_rendered text NOT NULL
);


--
-- Name: pylvax_app_feladat_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pylvax_app_feladat_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pylvax_app_feladat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pylvax_app_feladat_id_seq OWNED BY pylvax_app_feladat.id;


--
-- Name: pylvax_app_homeitem; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE pylvax_app_homeitem (
    id integer NOT NULL,
    slug character varying(50) NOT NULL,
    szoveg text NOT NULL,
    "order" integer,
    _szoveg_rendered text NOT NULL,
    url character varying(40) NOT NULL
);


--
-- Name: pylvax_app_homeitem_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pylvax_app_homeitem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pylvax_app_homeitem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pylvax_app_homeitem_id_seq OWNED BY pylvax_app_homeitem.id;


--
-- Name: pylvax_app_kepek; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE pylvax_app_kepek (
    id integer NOT NULL,
    slug character varying(50) NOT NULL,
    kep_file character varying(100) NOT NULL
);


--
-- Name: pylvax_app_kepek_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pylvax_app_kepek_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pylvax_app_kepek_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pylvax_app_kepek_id_seq OWNED BY pylvax_app_kepek.id;


--
-- Name: pylvax_app_menuitem; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE pylvax_app_menuitem (
    id integer NOT NULL,
    slug character varying(50) NOT NULL,
    text character varying(100) NOT NULL,
    "order" integer
);


--
-- Name: pylvax_app_menuitem_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pylvax_app_menuitem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pylvax_app_menuitem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pylvax_app_menuitem_id_seq OWNED BY pylvax_app_menuitem.id;


--
-- Name: pylvax_app_tartalom; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE pylvax_app_tartalom (
    id integer NOT NULL,
    slug character varying(50) NOT NULL,
    szoveg text NOT NULL,
    _szoveg_rendered text NOT NULL
);


--
-- Name: pylvax_app_tartalom_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pylvax_app_tartalom_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pylvax_app_tartalom_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pylvax_app_tartalom_id_seq OWNED BY pylvax_app_tartalom.id;


--
-- Name: pylvax_app_tematika; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE pylvax_app_tematika (
    id integer NOT NULL,
    datum date NOT NULL,
    slug character varying(50) NOT NULL,
    cim character varying(100) NOT NULL,
    leiras text NOT NULL,
    _leiras_rendered text NOT NULL
);


--
-- Name: pylvax_app_tematika_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pylvax_app_tematika_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pylvax_app_tematika_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pylvax_app_tematika_id_seq OWNED BY pylvax_app_tematika.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pylvax_app_blog ALTER COLUMN id SET DEFAULT nextval('pylvax_app_blog_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pylvax_app_feladat ALTER COLUMN id SET DEFAULT nextval('pylvax_app_feladat_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pylvax_app_homeitem ALTER COLUMN id SET DEFAULT nextval('pylvax_app_homeitem_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pylvax_app_kepek ALTER COLUMN id SET DEFAULT nextval('pylvax_app_kepek_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pylvax_app_menuitem ALTER COLUMN id SET DEFAULT nextval('pylvax_app_menuitem_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pylvax_app_tartalom ALTER COLUMN id SET DEFAULT nextval('pylvax_app_tartalom_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pylvax_app_tematika ALTER COLUMN id SET DEFAULT nextval('pylvax_app_tematika_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: -
--

COPY auth_group (id, name) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: -
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: -
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add permission	2	add_permission
5	Can change permission	2	change_permission
6	Can delete permission	2	delete_permission
7	Can add group	3	add_group
8	Can change group	3	change_group
9	Can delete group	3	delete_group
10	Can add user	4	add_user
11	Can change user	4	change_user
12	Can delete user	4	delete_user
13	Can add content type	5	add_contenttype
14	Can change content type	5	change_contenttype
15	Can delete content type	5	delete_contenttype
16	Can add session	6	add_session
17	Can change session	6	change_session
18	Can delete session	6	delete_session
19	Can add menu item	7	add_menuitem
20	Can change menu item	7	change_menuitem
21	Can delete menu item	7	delete_menuitem
22	Can add home item	8	add_homeitem
23	Can change home item	8	change_homeitem
24	Can delete home item	8	delete_homeitem
25	Can add tartalom	9	add_tartalom
26	Can change tartalom	9	change_tartalom
27	Can delete tartalom	9	delete_tartalom
28	Can add feladat	10	add_feladat
29	Can change feladat	10	change_feladat
30	Can delete feladat	10	delete_feladat
31	Can add tematika	11	add_tematika
32	Can change tematika	11	change_tematika
33	Can delete tematika	11	delete_tematika
34	Can add kepek	12	add_kepek
35	Can change kepek	12	change_kepek
36	Can delete kepek	12	delete_kepek
37	Can add blog	13	add_blog
38	Can change blog	13	change_blog
39	Can delete blog	13	delete_blog
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('auth_permission_id_seq', 39, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: -
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	pbkdf2_sha256$20000$mPYgao2YtGL7$796icTAWGfTcQZoAiIJ7EDG+qamytMGdbSj1Qo/c3nU=	2015-11-23 07:37:49.083867+00	t	pylvax				t	t	2015-09-21 12:07:17.311139+00
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: -
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('auth_user_id_seq', 1, true);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: -
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: -
--

COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2015-09-21 12:11:29.246772+00	1	Főoldal	1		7	1
2	2015-09-21 12:12:14.651502+00	2	Új vagy?	1		7	1
3	2015-09-21 12:12:38.5768+00	3	Tematika	1		7	1
4	2015-09-21 12:12:53.733595+00	4	Feladatok	1		7	1
5	2015-09-21 12:13:34.861952+00	5	Hasznos infó	1		7	1
6	2015-09-21 12:13:52.119568+00	6	Képek	1		7	1
7	2015-09-21 12:14:46.464413+00	7	Kapcsolat	1		7	1
8	2015-09-21 12:17:19.070584+00	1	Főoldal	2	slug módosítva.	7	1
9	2015-09-21 12:17:25.225666+00	2	Új vagy?	2	Egy mező sem változott.	7	1
10	2015-09-21 12:17:29.115715+00	3	Tematika	2	Egy mező sem változott.	7	1
11	2015-09-21 12:17:33.059089+00	4	Feladatok	2	Egy mező sem változott.	7	1
12	2015-09-21 12:17:37.236904+00	5	Hasznos infó	2	Egy mező sem változott.	7	1
13	2015-09-21 12:17:42.849552+00	7	Kapcsolat	2	Egy mező sem változott.	7	1
14	2015-09-21 12:20:51.760898+00	1	hello	1		8	1
15	2015-09-21 12:23:14.720312+00	2	uj_vagy	1		8	1
16	2015-09-21 12:24:43.411974+00	3	segits	1		8	1
17	2015-09-21 12:25:26.935083+00	4	tematika	1		8	1
18	2015-09-21 12:26:02.089987+00	5	feladatok	1		8	1
19	2015-09-21 12:28:24.383231+00	6	hasznos	1		8	1
20	2015-09-21 12:28:49.581876+00	5	feladatok	2	szoveg módosítva.	8	1
21	2015-09-21 12:29:11.187876+00	1	hello	2	szoveg és order módosítva.	8	1
22	2015-09-21 12:29:47.958336+00	2	uj_vagy	2	szoveg és order módosítva.	8	1
23	2015-09-21 12:30:13.466966+00	3	segits	2	szoveg és order módosítva.	8	1
24	2015-09-21 12:30:25.500018+00	4	tematika	2	szoveg és order módosítva.	8	1
25	2015-09-21 12:30:34.388762+00	5	feladatok	2	szoveg és order módosítva.	8	1
26	2015-09-21 12:30:42.712339+00	6	hasznos	2	szoveg és order módosítva.	8	1
27	2015-09-21 12:43:53.709786+00	1	hasznos	1		9	1
28	2015-09-21 12:52:19.848179+00	1	Home	2	text módosítva.	7	1
29	2015-09-21 12:52:56.187465+00	2	Newbies	2	slug és text módosítva.	7	1
30	2015-09-21 12:53:52.468948+00	3	Topics	2	slug és text módosítva.	7	1
31	2015-09-21 12:54:01.419998+00	4	Tasks	2	slug és text módosítva.	7	1
32	2015-09-21 12:55:04.968058+00	5	Resources	2	slug és text módosítva.	7	1
33	2015-09-21 12:55:32.146199+00	5	Info	2	slug és text módosítva.	7	1
34	2015-09-21 12:55:52.050316+00	6	Pics	2	slug és text módosítva.	7	1
35	2015-09-21 12:56:02.388295+00	7	Contact	2	slug és text módosítva.	7	1
36	2015-09-21 13:08:35.921226+00	1	hasznos	2	szoveg módosítva.	9	1
37	2015-09-21 13:10:31.230983+00	2	Newbies	2	slug módosítva.	7	1
38	2015-09-21 13:12:50.728044+00	1	hasznos	2	szoveg módosítva.	9	1
39	2015-09-21 13:13:00.535158+00	1	info	2	slug és szoveg módosítva.	9	1
40	2015-09-21 13:13:00.714883+00	1	hello	2	szoveg módosítva.	8	1
41	2015-09-21 13:16:59.187288+00	6	info	2	slug és szoveg módosítva.	8	1
42	2015-09-21 13:19:01.146013+00	2	newbie	2	slug és szoveg módosítva.	8	1
43	2015-09-21 13:19:35.725926+00	5	feladatok	2	szoveg módosítva.	8	1
44	2015-09-21 13:19:47.236435+00	5	tasks	2	slug és szoveg módosítva.	8	1
45	2015-09-21 13:23:34.613184+00	3	help	2	slug és szoveg módosítva.	8	1
46	2015-09-21 13:24:32.452234+00	4	topics	2	slug és szoveg módosítva.	8	1
47	2015-09-21 13:52:56.103674+00	1	home	2	slug és szoveg módosítva.	8	1
48	2015-09-21 14:16:50.090209+00	1	monty	1		12	1
49	2015-09-21 15:01:32.546531+00	2	newbie	1		9	1
50	2015-09-21 15:03:54.094207+00	2	newbie	2	szoveg módosítva.	9	1
51	2015-09-21 19:06:18.549653+00	2	newbie	2	szoveg módosítva.	8	1
52	2015-09-21 19:08:46.026317+00	2	newbie	2	szoveg módosítva.	9	1
53	2015-09-21 19:10:04.816959+00	2	newbie	2	szoveg módosítva.	9	1
54	2015-09-21 20:58:43.368689+00	1	09/16	1		10	1
55	2015-09-21 21:00:25.161865+00	2	09/02	1		10	1
56	2015-09-21 21:00:36.915383+00	1	09/16	2	slug, leiras és megoldas módosítva.	10	1
57	2015-09-21 21:23:42.388818+00	1	Beautiful Soup	1		11	1
58	2015-09-21 21:25:01.374409+00	2	09/16	1		11	1
59	2015-09-21 21:25:12.604465+00	1	09/02	2	cim és leiras módosítva.	11	1
60	2015-09-21 21:33:37.285356+00	2	JSON	2	cim és leiras módosítva.	11	1
61	2015-09-21 21:33:47.760249+00	1	BeautifulSoup	2	cim és leiras módosítva.	11	1
62	2015-09-21 21:34:07.750325+00	2	BeautifulSoup	2	cim, leiras és megoldas módosítva.	10	1
63	2015-09-21 21:34:18.623087+00	1	JSON	2	cim, leiras és megoldas módosítva.	10	1
64	2015-09-22 11:02:29.311496+00	1	info	2	szoveg módosítva.	9	1
65	2015-09-22 11:06:53.722296+00	1	info	2	szoveg módosítva.	9	1
66	2015-09-22 11:10:13.68375+00	1	info	2	szoveg módosítva.	9	1
67	2015-09-22 11:11:30.350884+00	1	info	2	szoveg módosítva.	9	1
68	2015-09-22 11:18:32.648315+00	1	info	2	szoveg módosítva.	9	1
69	2015-09-22 11:20:14.496114+00	1	info	2	szoveg módosítva.	9	1
70	2015-09-22 11:21:43.594834+00	1	info	2	szoveg módosítva.	9	1
71	2015-09-22 11:22:57.35134+00	1	info	2	szoveg módosítva.	9	1
72	2015-09-22 11:27:21.584404+00	1	info	2	szoveg módosítva.	9	1
73	2015-09-22 11:28:33.250237+00	1	info	2	szoveg módosítva.	9	1
74	2015-09-22 11:31:11.816671+00	1	info	2	szoveg módosítva.	9	1
75	2015-09-22 11:37:02.639647+00	1	info	2	szoveg módosítva.	9	1
76	2015-09-22 11:38:25.824149+00	1	info	2	szoveg módosítva.	9	1
77	2015-09-22 11:40:01.416851+00	1	info	2	szoveg módosítva.	9	1
78	2015-09-22 11:40:44.629573+00	1	info	2	szoveg módosítva.	9	1
79	2015-09-22 11:41:18.878506+00	1	info	2	szoveg módosítva.	9	1
80	2015-09-22 11:42:03.499585+00	1	info	2	szoveg módosítva.	9	1
81	2015-09-22 11:42:54.646754+00	1	info	2	szoveg módosítva.	9	1
82	2015-09-22 11:45:17.897007+00	1	info	2	szoveg módosítva.	9	1
83	2015-09-22 11:45:38.559932+00	1	info	2	szoveg módosítva.	9	1
84	2015-09-22 11:46:07.95898+00	1	info	2	szoveg módosítva.	9	1
85	2015-09-22 11:55:28.621967+00	1	info	2	szoveg módosítva.	9	1
86	2015-09-22 11:58:25.939384+00	1	info	2	szoveg módosítva.	9	1
87	2015-09-22 11:59:11.398254+00	1	info	2	szoveg módosítva.	9	1
88	2015-09-22 12:01:49.260434+00	1	info	2	szoveg módosítva.	9	1
89	2015-09-22 12:26:03.458715+00	1	monty	3		12	1
90	2015-09-22 12:27:08.746742+00	2	pylvax1	1		12	1
91	2015-09-22 12:27:20.970393+00	3	pylvax2	1		12	1
92	2015-09-22 12:27:31.025024+00	4	pylvax3	1		12	1
93	2015-09-22 12:27:39.083112+00	5	pylvax4	1		12	1
94	2015-09-22 12:27:49.158118+00	6	pylvax5	1		12	1
95	2015-11-13 17:02:54.760241+00	6	pylvax5	2	No fields changed.	12	1
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 95, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: -
--

COPY django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	pylvax_app	menuitem
8	pylvax_app	homeitem
9	pylvax_app	tartalom
10	pylvax_app	feladat
11	pylvax_app	tematika
12	pylvax_app	kepek
13	pylvax_app	blog
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('django_content_type_id_seq', 13, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: -
--

COPY django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2015-09-21 12:04:36.110162+00
2	auth	0001_initial	2015-09-21 12:04:36.191335+00
3	admin	0001_initial	2015-09-21 12:04:36.222689+00
4	contenttypes	0002_remove_content_type_name	2015-09-21 12:04:36.296052+00
5	auth	0002_alter_permission_name_max_length	2015-09-21 12:04:36.319499+00
6	auth	0003_alter_user_email_max_length	2015-09-21 12:04:36.353407+00
7	auth	0004_alter_user_username_opts	2015-09-21 12:04:36.373949+00
8	auth	0005_alter_user_last_login_null	2015-09-21 12:04:36.399247+00
9	auth	0006_require_contenttypes_0002	2015-09-21 12:04:36.401436+00
10	pylvax_app	0001_initial	2015-09-21 12:04:36.532562+00
11	sessions	0001_initial	2015-09-21 12:04:36.547281+00
12	pylvax_app	0002_homeitem_url	2015-10-06 22:29:12.886336+00
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('django_migrations_id_seq', 12, true);


--
-- Data for Name: pylvax_app_blog; Type: TABLE DATA; Schema: public; Owner: -
--

COPY pylvax_app_blog (id, datum, cim, leiras, kep, _leiras_rendered) FROM stdin;
\.


--
-- Name: pylvax_app_blog_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('pylvax_app_blog_id_seq', 1, false);


--
-- Data for Name: pylvax_app_feladat; Type: TABLE DATA; Schema: public; Owner: -
--

COPY pylvax_app_feladat (id, datum, slug, cim, leiras, megoldas, _megoldas_rendered, _leiras_rendered) FROM stdin;
2	2015-09-02	task0902	BeautifulSoup	Beautiful Soup	```\r\nfrom bs4 import BeautifulSoup\r\nimport codecs\r\n\r\n\r\ndef read_file_contents(path):\r\n    with codecs.open(path, encoding='utf-8') as infile:\r\n        return infile.read().strip()\r\n\r\nkiralyok_str = read_file_contents('kiralyok.html')\r\n\r\n\r\nsoup = BeautifulSoup(kiralyok_str, 'html.parser')\r\n\r\nh3_list = soup.find_all('h3')\r\nh3 = h3_list[0]\r\nkiralyhaz = h3.find('span').text\r\n\r\nkorszakok = list()\r\n\r\nfor h3 in h3_list:\r\n    span = h3.find('span', {'class': 'mw-headline'})\r\n    if span:\r\n        korszak = span.text\r\n        table = h3.next_sibling.next_sibling\r\n\r\n        korszakok.append((korszak, table))\r\n\r\n```\r\n	<pre><code>from bs4 import BeautifulSoup\nimport codecs\n\n\ndef read_file_contents(path):\n    with codecs.open(path, encoding='utf-8') as infile:\n        return infile.read().strip()\n\nkiralyok_str = read_file_contents('kiralyok.html')\n\n\nsoup = BeautifulSoup(kiralyok_str, 'html.parser')\n\nh3_list = soup.find_all('h3')\nh3 = h3_list[0]\nkiralyhaz = h3.find('span').text\n\nkorszakok = list()\n\nfor h3 in h3_list:\n    span = h3.find('span', {'class': 'mw-headline'})\n    if span:\n        korszak = span.text\n        table = h3.next_sibling.next_sibling\n\n        korszakok.append((korszak, table))\n\n</code></pre>\n	<p>Beautiful Soup</p>\n
1	2015-09-16	task0916	JSON	JSON forecast	```\r\nimport requests\r\n\r\nlongitude = 19.05\r\nlatitude = 47.53\r\napikey = '2e4f0d2961984011b1eaf8ad0ca8489d'\r\n\r\nurl_template = 'https://api.forecast.io/forecast/{api}/{lat},{lon}'\r\nurl = url_template.format(api=apikey, lat=latitude, lon=longitude)\r\n\r\nprint url\r\n\r\nr = requests.get(url)\r\nunits = 'si'\r\n\r\nif r.ok:\r\n    data = r.json()\r\n\r\n\r\nr2 = requests.get(url + '?units=si')\r\nif r2.ok:\r\n    data_si = r.json()\r\n\r\nparams_dict = {\r\n    'units': 'si',\r\n    'lang': 'fr'\r\n}\r\n\r\nr3 = requests.get(url, params=params_dict)\r\nif r3.ok:\r\n    data_si2 = r.json()\r\n```\r\n	<pre><code>import requests\n\nlongitude = 19.05\nlatitude = 47.53\napikey = '2e4f0d2961984011b1eaf8ad0ca8489d'\n\nurl_template = 'https://api.forecast.io/forecast/{api}/{lat},{lon}'\nurl = url_template.format(api=apikey, lat=latitude, lon=longitude)\n\nprint url\n\nr = requests.get(url)\nunits = 'si'\n\nif r.ok:\n    data = r.json()\n\n\nr2 = requests.get(url + '?units=si')\nif r2.ok:\n    data_si = r.json()\n\nparams_dict = {\n    'units': 'si',\n    'lang': 'fr'\n}\n\nr3 = requests.get(url, params=params_dict)\nif r3.ok:\n    data_si2 = r.json()\n</code></pre>\n	<p>JSON forecast</p>\n
\.


--
-- Name: pylvax_app_feladat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('pylvax_app_feladat_id_seq', 2, true);


--
-- Data for Name: pylvax_app_homeitem; Type: TABLE DATA; Schema: public; Owner: -
--

COPY pylvax_app_homeitem (id, slug, szoveg, "order", _szoveg_rendered, url) FROM stdin;
6	info	What else?\r\n--------------\r\nYou can continue learning at home, we collected useful stuffs, great tutorials and books.\r\n	6	<h2>What else?</h2>\n<p>You can continue learning at home, we collected useful stuffs, great tutorials and books.</p>\n	
5	tasks	What we did so far\r\n---------------------\r\nYou can find here the materials of past lessons and homeworks. \r\n	5	<h2>What we did so far</h2>\n<p>You can find here the materials of past lessons and homeworks.</p>\n	
3	help	You can also help!\r\n-------------\r\nIf you are a coding ninja, or a developer with some experiences, you are also welcome. <br>\r\nGive lessons, help the beginners, and perhaps you will also learn something new!\r\n	3	<h2>You can also help!</h2>\n<p>If you are a coding ninja, or a developer with some experiences, you are also welcome. <br>\nGive lessons, help the beginners, and perhaps you will also learn something new!</p>\n	
4	topics	What we learn\r\n-------------\r\nWe are learning in two separated groups, Dániel Ábel helps to the newcomes to get into the flow.</br>\r\nZsolti Erő shows more and more magic to the old hands.</br>\r\nThe details you can find here.\r\n	4	<h2>What we learn</h2>\n<p>We are learning in two separated groups, Dániel Ábel helps to the newcomes to get into the flow.</br>\nZsolti Erő shows more and more magic to the old hands.</br>\nThe details you can find here.</p>\n	
1	home	Hello!\r\n------\r\nWelcome! Pylvax is a workshop, where you can learn coding even if you are totally beginner and haven't seen any lines of code before. </br>\r\nWe meet every wednesday, we code together and share our experiences with each other.\r\n	1	<h2>Hello!</h2>\n<p>Welcome! Pylvax is a workshop, where you can learn coding even if you are totally beginner and haven't seen any lines of code before. </br>\nWe meet every wednesday, we code together and share our experiences with each other.</p>\n	
2	newbie	Don't know how to code? No problem!\r\n-----------------------------\r\nWe don't expect you to have any previous experience in coding or in the IT field.<br>\r\nYou can take the first steps even at home!\r\n	2	<h2>Don't know how to code? No problem!</h2>\n<p>We don't expect you to have any previous experience in coding or in the IT field.<br>\nYou can take the first steps even at home!</p>\n	
\.


--
-- Name: pylvax_app_homeitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('pylvax_app_homeitem_id_seq', 6, true);


--
-- Data for Name: pylvax_app_kepek; Type: TABLE DATA; Schema: public; Owner: -
--

COPY pylvax_app_kepek (id, slug, kep_file) FROM stdin;
2	pylvax1	gallery_kepek/pylvax1.jpg
3	pylvax2	gallery_kepek/pylvax2.jpg
4	pylvax3	gallery_kepek/pylvax3.jpg
5	pylvax4	gallery_kepek/pylvax4.jpg
6	pylvax5	gallery_kepek/pylvax5.jpg
\.


--
-- Name: pylvax_app_kepek_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('pylvax_app_kepek_id_seq', 6, true);


--
-- Data for Name: pylvax_app_menuitem; Type: TABLE DATA; Schema: public; Owner: -
--

COPY pylvax_app_menuitem (id, slug, text, "order") FROM stdin;
1	home	Home	1
3	topics	Topics	3
4	tasks	Tasks	4
5	info	Info	5
6	pics	Pics	6
7	contact	Contact	7
2	newbie	Newbies	2
\.


--
-- Name: pylvax_app_menuitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('pylvax_app_menuitem_id_seq', 7, true);


--
-- Data for Name: pylvax_app_tartalom; Type: TABLE DATA; Schema: public; Owner: -
--

COPY pylvax_app_tartalom (id, slug, szoveg, _szoveg_rendered) FROM stdin;
2	newbie	First steps\r\n==================\r\nPay attention to your assignments, practice at home, talk with developers and you'll see that your skills will improve sooner than you would think!\r\n\r\nYou are gonna need to install the following stuffs for getting started:\r\n\r\n**Windows**\r\n\r\n**Python** (use 32-bit)</br>\r\n[https://www.python.org/ftp/python/2.7.9/python-2.7.9.msi](http://)\r\n\r\n**Sublime Text 2** (use 32-bit)</br>\r\n[http://c758482.r82.cf2.rackcdn.com/Sublime%20Text%202.0.2%20Setup.exe](http://)</br></br>\r\nTotal Commander</br>\r\n[http://totalcmd2.s3.amazonaws.com/tcmd851ax32_64.exe](http://)</br></br>\r\n\r\n**Cmder**</br>\r\n[https://github.com/bliker/cmder/releases/download/v1.1.4.1/cmder.zip](http://)</br></br>\r\n\r\n**Path Editor**</br>\r\n[http://rix0r.nl/downloads/windowspatheditor/windowspatheditor-1.3.zip](http://)\r\n</br></br>\r\n\r\n**Ipython**</br>\r\nipython‑3.0.0‑py27‑none‑any.whl</br>\r\n[http://www.lfd.uci.edu/~gohlke/pythonlibs/#ipython](http://)\r\n</br></br>\r\n	<h1>First steps</h1>\n<p>Pay attention to your assignments, practice at home, talk with developers and you'll see that your skills will improve sooner than you would think!</p>\n<p>You are gonna need to install the following stuffs for getting started:</p>\n<p><strong>Windows</strong></p>\n<p><strong>Python</strong> (use 32-bit)</br>\n<a href="http://">https://www.python.org/ftp/python/2.7.9/python-2.7.9.msi</a></p>\n<p><strong>Sublime Text 2</strong> (use 32-bit)</br>\n<a href="http://">http://c758482.r82.cf2.rackcdn.com/Sublime%20Text%202.0.2%20Setup.exe</a></br></br>\nTotal Commander</br>\n<a href="http://">http://totalcmd2.s3.amazonaws.com/tcmd851ax32_64.exe</a></br></br></p>\n<p><strong>Cmder</strong></br>\n<a href="http://">https://github.com/bliker/cmder/releases/download/v1.1.4.1/cmder.zip</a></br></br></p>\n<p><strong>Path Editor</strong></br>\n<a href="http://">http://rix0r.nl/downloads/windowspatheditor/windowspatheditor-1.3.zip</a>\n</br></br></p>\n<p><strong>Ipython</strong></br>\nipython‑3.0.0‑py27‑none‑any.whl</br>\n<a href="http://">http://www.lfd.uci.edu/~gohlke/pythonlibs/#ipython</a>\n</br></br></p>\n
1	info	<p class="info_title">Here we collected many information for you to help learning.</p>\r\n</br>\r\n<p class="y_line">\r\nIntro to Computer Science\r\n</p>\r\nFirst an amazing course, which explains the very basics of programming and Python language:</br>\r\n\r\n[www.udacity.com/course/cs101](https://www.udacity.com/course/cs101)\r\n\r\n<p class="b_line">\r\nCodecademy / Python</p>\r\n\r\nCodecademy is useful to practice the newly learned stuffs.</br>\r\n[www.codecademy.com/tracks/python](https://www.codecademy.com/tracks/python)\r\n\r\n<p class="y_line">\r\nLearn Python the hard way</p>\r\n\r\nA great online book teaches you from fundamentals, shows how to use command line, and helps to take the first steps.</br>\r\n The language it is written, awesome! :)</br>\r\n[learnpythonthehardway.org/book/](http://learnpythonthehardway.org/book/)\r\n\r\n<p class="b_line">Programming Foundations with Python</p>\r\n\r\nIf you want to know more about object oriented programming, you can start with simple but great exercises.</br>\r\n[www.udacity.com/course/ud036](http://www.udacity.com/course/ud036)\r\n\r\n<p class="y_line">Exercism</p>\r\n\r\nThe idea behind the site is pretty simple: write code for all kinds of problems, submit it, get feedback for it from other users, and give feedback to them. You'll get to see how others solve problems.</br>\r\n[exercism.io](http://exercism.io/)\r\n\r\n<p class="b_line">Intro to Git and Github</p>\r\nThis course can teach you to understand version control and shows how to collaborate with other developers.</br>\r\n\r\n[www.udacity.com/course/how-to-use-git-and-github](http://www.udacity.com/course/how-to-use-git-and-github--ud775)\r\n\r\n<p class="y_line">\r\nAn Introduction to Interactive Programming in Python</p>\r\nIt is  partially enabled course, you have to check, when it is opened, but also good, and from basics.</br>\r\n\r\n[www.coursera.org/course/interactivepython1\r\n](http://www.coursera.org/course/interactivepython1)\r\n\r\nIt has a second part as well:</br>\r\n[www.coursera.org/course/interactivepython2](http://www.coursera.org/course/interactivepython2)\r\n\r\n\r\n	<p class="info_title">Here we collected many information for you to help learning.</p>\n</br>\n<p class="y_line">\nIntro to Computer Science\n</p>\nFirst an amazing course, which explains the very basics of programming and Python language:</br>\n<p><a href="https://www.udacity.com/course/cs101">www.udacity.com/course/cs101</a></p>\n<p class="b_line">\nCodecademy / Python</p>\n<p>Codecademy is useful to practice the newly learned stuffs.</br>\n<a href="https://www.codecademy.com/tracks/python">www.codecademy.com/tracks/python</a></p>\n<p class="y_line">\nLearn Python the hard way</p>\n<p>A great online book teaches you from fundamentals, shows how to use command line, and helps to take the first steps.</br>\nThe language it is written, awesome! :)</br>\n<a href="http://learnpythonthehardway.org/book/">learnpythonthehardway.org/book/</a></p>\n<p class="b_line">Programming Foundations with Python</p>\n<p>If you want to know more about object oriented programming, you can start with simple but great exercises.</br>\n<a href="http://www.udacity.com/course/ud036">www.udacity.com/course/ud036</a></p>\n<p class="y_line">Exercism</p>\n<p>The idea behind the site is pretty simple: write code for all kinds of problems, submit it, get feedback for it from other users, and give feedback to them. You'll get to see how others solve problems.</br>\n<a href="http://exercism.io/">exercism.io</a></p>\n<p class="b_line">Intro to Git and Github</p>\nThis course can teach you to understand version control and shows how to collaborate with other developers.</br>\n<p><a href="http://www.udacity.com/course/how-to-use-git-and-github--ud775">www.udacity.com/course/how-to-use-git-and-github</a></p>\n<p class="y_line">\nAn Introduction to Interactive Programming in Python</p>\nIt is  partially enabled course, you have to check, when it is opened, but also good, and from basics.</br>\n<p><a href="http://www.coursera.org/course/interactivepython1">www.coursera.org/course/interactivepython1\n</a></p>\n<p>It has a second part as well:</br>\n<a href="http://www.coursera.org/course/interactivepython2">www.coursera.org/course/interactivepython2</a></p>\n
\.


--
-- Name: pylvax_app_tartalom_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('pylvax_app_tartalom_id_seq', 2, true);


--
-- Data for Name: pylvax_app_tematika; Type: TABLE DATA; Schema: public; Owner: -
--

COPY pylvax_app_tematika (id, datum, slug, cim, leiras, _leiras_rendered) FROM stdin;
2	2015-09-16	topic0916	JSON	With JSON you can structure your data.	<p>With JSON you can structure your data.</p>\n
1	2015-09-02	topic0902	BeautifulSoup	With BeautifulSoup you can scrape the html and get huge amount of data.	<p>With BeautifulSoup you can scrape the html and get huge amount of data.</p>\n
\.


--
-- Name: pylvax_app_tematika_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('pylvax_app_tematika_id_seq', 2, true);


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_key UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_codename_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_key UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_group_id_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_key UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_key UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_45f3b1d93ec8c61c_uniq; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_45f3b1d93ec8c61c_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: pylvax_app_blog_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pylvax_app_blog
    ADD CONSTRAINT pylvax_app_blog_pkey PRIMARY KEY (id);


--
-- Name: pylvax_app_feladat_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pylvax_app_feladat
    ADD CONSTRAINT pylvax_app_feladat_pkey PRIMARY KEY (id);


--
-- Name: pylvax_app_feladat_slug_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pylvax_app_feladat
    ADD CONSTRAINT pylvax_app_feladat_slug_key UNIQUE (slug);


--
-- Name: pylvax_app_homeitem_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pylvax_app_homeitem
    ADD CONSTRAINT pylvax_app_homeitem_pkey PRIMARY KEY (id);


--
-- Name: pylvax_app_homeitem_slug_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pylvax_app_homeitem
    ADD CONSTRAINT pylvax_app_homeitem_slug_key UNIQUE (slug);


--
-- Name: pylvax_app_kepek_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pylvax_app_kepek
    ADD CONSTRAINT pylvax_app_kepek_pkey PRIMARY KEY (id);


--
-- Name: pylvax_app_kepek_slug_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pylvax_app_kepek
    ADD CONSTRAINT pylvax_app_kepek_slug_key UNIQUE (slug);


--
-- Name: pylvax_app_menuitem_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pylvax_app_menuitem
    ADD CONSTRAINT pylvax_app_menuitem_pkey PRIMARY KEY (id);


--
-- Name: pylvax_app_menuitem_slug_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pylvax_app_menuitem
    ADD CONSTRAINT pylvax_app_menuitem_slug_key UNIQUE (slug);


--
-- Name: pylvax_app_tartalom_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pylvax_app_tartalom
    ADD CONSTRAINT pylvax_app_tartalom_pkey PRIMARY KEY (id);


--
-- Name: pylvax_app_tartalom_slug_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pylvax_app_tartalom
    ADD CONSTRAINT pylvax_app_tartalom_slug_key UNIQUE (slug);


--
-- Name: pylvax_app_tematika_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pylvax_app_tematika
    ADD CONSTRAINT pylvax_app_tematika_pkey PRIMARY KEY (id);


--
-- Name: pylvax_app_tematika_slug_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pylvax_app_tematika
    ADD CONSTRAINT pylvax_app_tematika_slug_key UNIQUE (slug);


--
-- Name: auth_group_name_253ae2a6331666e8_like; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auth_group_name_253ae2a6331666e8_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_0e939a4f; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auth_group_permissions_0e939a4f ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_8373b171; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auth_group_permissions_8373b171 ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_417f1b1c; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auth_permission_417f1b1c ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_0e939a4f; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auth_user_groups_0e939a4f ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_e8701ad4; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auth_user_groups_e8701ad4 ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_8373b171; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_8373b171 ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_e8701ad4; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_e8701ad4 ON auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_51b3b110094b8aae_like; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auth_user_username_51b3b110094b8aae_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_417f1b1c; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX django_admin_log_417f1b1c ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_e8701ad4; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX django_admin_log_e8701ad4 ON django_admin_log USING btree (user_id);


--
-- Name: django_session_de54fa62; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX django_session_de54fa62 ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_461cfeaa630ca218_like; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX django_session_session_key_461cfeaa630ca218_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: pylvax_app_feladat_slug_432dc727f62aa986_like; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX pylvax_app_feladat_slug_432dc727f62aa986_like ON pylvax_app_feladat USING btree (slug varchar_pattern_ops);


--
-- Name: pylvax_app_homeitem_slug_10ea7e995e676944_like; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX pylvax_app_homeitem_slug_10ea7e995e676944_like ON pylvax_app_homeitem USING btree (slug varchar_pattern_ops);


--
-- Name: pylvax_app_kepek_slug_56b5e88aefd10d9f_like; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX pylvax_app_kepek_slug_56b5e88aefd10d9f_like ON pylvax_app_kepek USING btree (slug varchar_pattern_ops);


--
-- Name: pylvax_app_menuitem_slug_523500fc103527f0_like; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX pylvax_app_menuitem_slug_523500fc103527f0_like ON pylvax_app_menuitem USING btree (slug varchar_pattern_ops);


--
-- Name: pylvax_app_tartalom_slug_48e09f2003a9ff58_like; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX pylvax_app_tartalom_slug_48e09f2003a9ff58_like ON pylvax_app_tartalom USING btree (slug varchar_pattern_ops);


--
-- Name: pylvax_app_tematika_slug_12a667d48da646_like; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX pylvax_app_tematika_slug_12a667d48da646_like ON pylvax_app_tematika USING btree (slug varchar_pattern_ops);


--
-- Name: auth_content_type_id_508cf46651277a81_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_content_type_id_508cf46651277a81_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissio_group_id_689710a9a73b7457_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_group_id_689710a9a73b7457_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user__permission_id_384b62483d7071f0_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user__permission_id_384b62483d7071f0_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permiss_user_id_7f0938558328534a_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permiss_user_id_7f0938558328534a_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djan_content_type_id_697914295151027a_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT djan_content_type_id_697914295151027a_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

