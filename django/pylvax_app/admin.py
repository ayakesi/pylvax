from django.contrib import admin
from .models import MenuItem, Tartalom, Feladat, Kepek, Blog, Tematika, HomeItem

admin.site.register(MenuItem)
admin.site.register(HomeItem)
admin.site.register(Tartalom)
admin.site.register(Feladat)
admin.site.register(Kepek)
admin.site.register(Blog)
admin.site.register(Tematika)




