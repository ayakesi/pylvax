# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import pylvax_app.models
import markitup.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Blog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('datum', models.DateTimeField(auto_now_add=True)),
                ('cim', models.CharField(max_length=100)),
                ('leiras', markitup.fields.MarkupField(no_rendered_field=True, blank=True)),
                ('kep', models.FileField(upload_to=pylvax_app.models.blog_kepek_filename, blank=True)),
                ('_leiras_rendered', models.TextField(editable=False, blank=True)),
            ],
            options={
                'ordering': ['-datum'],
            },
        ),
        migrations.CreateModel(
            name='Feladat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('datum', models.DateField()),
                ('slug', models.SlugField(unique=True)),
                ('cim', models.CharField(max_length=100)),
                ('leiras', markitup.fields.MarkupField(no_rendered_field=True, blank=True)),
                ('megoldas', markitup.fields.MarkupField(no_rendered_field=True, blank=True)),
                ('_megoldas_rendered', models.TextField(editable=False, blank=True)),
                ('_leiras_rendered', models.TextField(editable=False, blank=True)),
            ],
            options={
                'ordering': ['-datum'],
            },
        ),
        migrations.CreateModel(
            name='HomeItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slug', models.SlugField(unique=True)),
                ('szoveg', markitup.fields.MarkupField(no_rendered_field=True)),
                ('order', models.IntegerField(null=True, blank=True)),
                ('_szoveg_rendered', models.TextField(editable=False, blank=True)),
            ],
            options={
                'ordering': ['order'],
            },
        ),
        migrations.CreateModel(
            name='Kepek',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slug', models.SlugField(unique=True)),
                ('kep_file', models.FileField(upload_to=pylvax_app.models.gallery_kepek_filename, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='MenuItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slug', models.SlugField(unique=True)),
                ('text', models.CharField(max_length=100)),
                ('order', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'ordering': ['order'],
            },
        ),
        migrations.CreateModel(
            name='Tartalom',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slug', models.SlugField(unique=True)),
                ('szoveg', markitup.fields.MarkupField(no_rendered_field=True, blank=True)),
                ('_szoveg_rendered', models.TextField(editable=False, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Tematika',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('datum', models.DateField()),
                ('slug', models.SlugField(unique=True)),
                ('cim', models.CharField(max_length=100)),
                ('leiras', markitup.fields.MarkupField(no_rendered_field=True, blank=True)),
                ('_leiras_rendered', models.TextField(editable=False, blank=True)),
            ],
            options={
                'ordering': ['-datum'],
            },
        ),
    ]
