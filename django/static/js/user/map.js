function initialize() {
    var pos_x_y = new google.maps.LatLng(47.4982420,19.0554940);

    var mapOptions = {
        zoom: 17,
        center: pos_x_y,
        disableDefaultUI: true,
        scrollwheel: false,
        zoomControl: true,
        zoomControlOptions: {
            position: google.maps.ControlPosition.LEFT_CENTER
        },
    };
    var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);


    var image = "/static/images/map-marker.png";

    var marker = new google.maps.Marker({
        position: pos_x_y,
        map: map,
        icon: image
    });

}

google.maps.event.addDomListener(window, "load", initialize);
